Hashable
========

(Language: EN+FR)

Hashable Calculator is scripts python to calcul hashes sums

Author
======
Stéphane HUC : <devs@stephane-huc.net><br />
GIT : https://git.framasoft.org/hucste/Hashable

---

USE
====
1/ make script Hashable executable!<br />
2/ IMPERATIVE: run-it in terminal-console.<br />
3/ answer questions :p<br />

Config
======
It's possible to use file config.<br />
Please, see files in folder /conf<br />

Enjoy-it!
=========
